package org.cdlflex.cdoperformance.test;

import java.sql.SQLException;

import org.cdlflex.cdoperformance.client.MyCdoClient;
import org.cdlflex.cdoperformance.model.CAEX.CaexPackage;
import org.cdlflex.cdoperformance.server.MyCdoServer;
import org.eclipse.net4j.db.DBException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class CdoEmbeddedPerformanceTest {
    private final MyCdoServer server = new MyCdoServer();
    private final MyCdoClient client = new MyCdoClient("repo");

    @Before
    public void setup() {
        server.start();
        client.connect();
        client.addPackage(CaexPackage.eINSTANCE);
    }

    @After
    public void tearDown() {
        client.disconnect();
        server.stop();
        try {
            server.purge();
        } catch (DBException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void testPerformance() {

    }
}
