package org.cdlflex.cdoperformance.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.cdlflex.cdoperformance.model.CAEX.Attribute;
import org.cdlflex.cdoperformance.model.CAEX.CAEXFile;
import org.cdlflex.cdoperformance.model.CAEX.CAEXObject;
import org.cdlflex.cdoperformance.model.CAEX.InstanceHierarchy;
import org.cdlflex.cdoperformance.model.CAEX.util.AmlReader;
import org.eclipse.emf.cdo.common.revision.CDORevision;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

final class CdoTestHelper {
    private static int idSuffix = 0;

    CdoTestHelper() {
        //
    }

    static void createTestModel(final CDOResource resource) {
        if (resource.eContents().isEmpty()) {
            try (InputStream in = CdoTestHelper.class.getClassLoader().getResourceAsStream("resources/largefile.aml")) {
                CAEXFile amlModel = AmlReader.getAmlModel(in);
                long objectCount = 0;
                TreeIterator<EObject> iterator = amlModel.eAllContents();
                while (iterator.hasNext()) {
                    iterator.next();
                    objectCount++;
                }
                System.out.println("   objects in model: " + objectCount);
                resource.getContents().add(amlModel);
            } catch (IOException e) {
                //
            }
        }
    }

    static void addInstanceHierarchyToTestModel(final CDOResource resource) {
        CAEXFile amlModel = measure("  load resource", () -> (CAEXFile) resource.getContents().get(0));
        InstanceHierarchy instanceHierarchy =
            measure("  access instance hierarchy", () -> amlModel.getInstanceHierarchy().get(0));
        InstanceHierarchy clone = measure("  clone", () -> cloneInstanceHierarchy(instanceHierarchy));
        measure("  add clone to list", v -> amlModel.getInstanceHierarchy().add(clone));
    }

    static void addInstanceHierarchyToTestModel(final CDOTransaction transaction) {
        InstanceHierarchy instanceHierarchy = measure("  query instance hierarchy", () -> {
            CDOQuery query =
                transaction.createQuery("ocl", "InstanceHierarchy.allInstances()->select(h | h.name='RMRFSTMLXV')",
                        null);
            query.setParameter("cdoLazyExtents", false);
            return query.getResultValue(InstanceHierarchy.class);
        });
        InstanceHierarchy clone = measure("  clone", () -> cloneInstanceHierarchy(instanceHierarchy));
        CAEXFile caexFile = measure("  get caexFile", () -> (CAEXFile) instanceHierarchy.eContainer());
        measure("  add clone to list", v -> caexFile.getInstanceHierarchy().add(clone));
    }

    static void updateTestModel(final CDOResource resource) {
        CAEXFile amlModel = measure("  load resource", () -> (CAEXFile) resource.getContents().get(0));
//        amlModel.cdoPrefetch(CDORevision.DEPTH_INFINITE);
        InstanceHierarchy instanceHierarchy =
            measure("  access instance hierarchy", () -> amlModel.getInstanceHierarchy().get(0));
        measure("  cdoPrefetch", v -> instanceHierarchy.cdoPrefetch(CDORevision.DEPTH_INFINITE));
        measure("  modified", v -> modifyInstanceHierarchy(instanceHierarchy));
    }

    static void updateTestModel(final CDOTransaction transaction) {
        InstanceHierarchy instanceHierarchy = measure("  query instance hierarchy", () -> {
            CDOQuery query =
                transaction.createQuery("ocl", "InstanceHierarchy.allInstances()",
                        null);
            query.setParameter("cdoLazyExtents", false);
            return query.getResultValue(InstanceHierarchy.class);
        });
        measure("  modified", v -> modifyInstanceHierarchy(instanceHierarchy));
    }

    private static InstanceHierarchy cloneInstanceHierarchy(InstanceHierarchy instanceHierarchy) {
        idSuffix++;
        InstanceHierarchy clone = EcoreUtil.copy(instanceHierarchy);
        clone.setID(clone.getID() + "-" + idSuffix);
        long objectCount = 0;
        TreeIterator<EObject> iterator = clone.eAllContents();
        while (iterator.hasNext()) {
            objectCount++;
            EObject eObject = iterator.next();
            if (eObject instanceof CAEXObject) {
                CAEXObject caexObject = (CAEXObject) eObject;
                caexObject.setID(caexObject.getID() + "-" + idSuffix);
            }
        }
        System.out.println("    objects in InstanceHierarchy: " + objectCount);
        return clone;
    }

    private static void modifyInstanceHierarchy(InstanceHierarchy instanceHierarchy) {
        Random rng = new Random(System.currentTimeMillis());
        long count = 0;
        TreeIterator<EObject> iterator = instanceHierarchy.eAllContents();
        while (iterator.hasNext()) {
            EObject eObject = iterator.next();
            if (eObject instanceof Attribute) {
                count++;
                ((Attribute) eObject).setValue(Integer.toString(rng.nextInt()));
            }
        }
        System.out.println("    attributes updated: " + count);
    }

    private static void measure(String actionDescription, Consumer<Void> action) {
        long start = System.currentTimeMillis();
        action.accept(null);
        System.out.format("%s: %,7d ms%n", actionDescription, System.currentTimeMillis() - start);
    }

    private static <T> T measure(String actionDescription, Supplier<T> supplier) {
        long start = System.currentTimeMillis();
        try {
            return supplier.get();
        } finally {
            System.out.format("%s: %,7d ms%n", actionDescription, System.currentTimeMillis() - start);
        }
    }

    static void printMemoryUsage(String prefix) {
        Runtime runtime = Runtime.getRuntime();
        long mb = 1024 * 1024;
        System.out.format("%25s: memory max: %,6dMB total %,6dMB free %,6dMB%n",
                          prefix,
                          runtime.maxMemory() / mb,
                          runtime.totalMemory() / mb,
                          runtime.freeMemory() / mb);
    }
}
