package org.cdlflex.cdoperformance.test;

import org.cdlflex.cdoperformance.client.MyCdoClient;
import org.cdlflex.cdoperformance.model.CAEX.CaexPackage;
import org.junit.Ignore;
import org.junit.Test;

public class CdoPerformanceTest {

    private final MyCdoClient client = new MyCdoClient("repo");

    @Test
    @Ignore
    public void testAddDataWithResourceLoad() {
        client.connect();
        client.addPackage(CaexPackage.eINSTANCE);

        client.commit("commit v0", "plant", CdoTestHelper::createTestModel);

        for (int i = 1; i <= 10; i++) {
            client.commit("commit v" + i, "plant", CdoTestHelper::addInstanceHierarchyToTestModel);
        }
    }

    @Test
    @Ignore
    public void testAddDataWithQuery() {
        client.connect();
        client.addPackage(CaexPackage.eINSTANCE);

        client.commit("commit v0", "plant", CdoTestHelper::createTestModel);

        for (int i = 1; i <= 10; i++) {
            client.commit("commit v" + i, CdoTestHelper::addInstanceHierarchyToTestModel);
        }
    }

    @Test
    @Ignore
    public void testUpdateDataWithResourceLoad() {
        client.connect();
        client.addPackage(CaexPackage.eINSTANCE);

        CdoTestHelper.printMemoryUsage("before initial commit");
        client.commit("commit v0", "plant", CdoTestHelper::createTestModel);
        CdoTestHelper.printMemoryUsage("after initial commit");

        for (int i = 1; i <= 20; i++) {
            client.commit("commit v" + i, "plant", CdoTestHelper::updateTestModel);
            CdoTestHelper.printMemoryUsage("after commit #" + i);
        }
    }

    @Test
    @Ignore
    public void testUpdateDataWithQuery() {
        client.connect();
        client.addPackage(CaexPackage.eINSTANCE);

        client.commit("commit v0", "plant", CdoTestHelper::createTestModel);

        for (int i = 1; i <= 20; i++) {
            client.commit("commit v" + i, CdoTestHelper::updateTestModel);
        }
    }
}
