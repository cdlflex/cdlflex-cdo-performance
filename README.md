# README #

### What is this repository for? ###

This project checks the performance of CDO with large CAEX models.

### How do I get set up? ###

* clone the project
* run mvn clean install
* import project into eclipse (Import maven project)
* start the CdoServerApp
    * create a run configuration: Eclipse Application. use org.cdlflex.cdoperformance.serverapp as the application to run
    * optionally, assign more memory
* start the performance test
    * open the CdoPerformanceTest class and remove the Ignore annotation from the first test method (testAddDataWithResourceLoad) or the third test method (testUpdateDataWithResourceLoad)
    * start the JUnit test (Run as JUnit Plugin Test)
    * optionally, assign more memory
* before a new run delete the H2 database (~/repo.h2.db)