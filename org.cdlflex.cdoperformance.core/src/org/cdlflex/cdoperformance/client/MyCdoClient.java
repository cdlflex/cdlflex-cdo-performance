package org.cdlflex.cdoperformance.client;

import java.util.List;
import java.util.function.Consumer;

import org.eclipse.emf.cdo.common.commit.CDOCommitHistory;
import org.eclipse.emf.cdo.common.commit.CDOCommitInfo;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.net4j.CDONet4jSession;
import org.eclipse.emf.cdo.net4j.CDONet4jSessionConfiguration;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CDOUtil;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.view.CDOAdapterPolicy;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.util.WrappedException;
import org.eclipse.net4j.util.container.IPluginContainer;

public class MyCdoClient {

    private final String repository;
    private CDONet4jSession session;
    private CDOView view;

    public MyCdoClient(final String repository) {
        this.repository = repository;
    }

    public void connect() {
        IConnector connector = Net4jUtil.getConnector(IPluginContainer.INSTANCE, "tcp", "localhost:22233");

        CDONet4jSessionConfiguration config = CDONet4jUtil.createNet4jSessionConfiguration();
        config.setConnector(connector);
        config.setRepositoryName(repository);
        config.setUserID("stef");
        config.setSignalTimeout(300000);

        session = config.openNet4jSession();
        session.options().setCollectionLoadingPolicy (CDOUtil.createCollectionLoadingPolicy(0, 300));
        session.options().setCommitTimeout(120);

        view = session.openView();
        view.options().addChangeSubscriptionPolicy(CDOAdapterPolicy.ALL);
    }

    public CDOCommitHistory getHistory() {
        return session.getCommitInfoManager().getHistory();
    }

    public CDOView getView() {
        return isConnected() ? view : null;
    }

    public CDOView getView(final CDOCommitInfo commit) {
        return session.openView(commit.getBranch(), commit.getTimeStamp());
    }

    public <T> List<T> query(final CDOView view, final String queryString, final Object context, final Class<T> resultType) {
        CDOQuery query = view.createQuery("ocl", queryString, context);
        return query.getResult(resultType);
    }

    public <T> List<T> query(final String queryString, final Object context, final Class<T> resultType) {
        return query(view, queryString, context, resultType);
    }

    public <T> T queryValue(final String queryString, final Object context, final Class<T> resultType) {
        CDOQuery query = view.createQuery("ocl", queryString, context);
        query.setParameter("cdoLazyExtents", false);
        return query.getResultValue(resultType);
    }

    public CDOCommitInfo commit(final String commitMessage, final String resourcePath, Consumer<CDOResource> resourceUpdater) {
        long startCommit = System.currentTimeMillis();
        long totalResourceUpdate = 0;
        CDOTransaction transaction = session.openTransaction();
        try {
            CDOResource resource = transaction.getOrCreateResource(resourcePath);
            long startResourceUpdate = System.currentTimeMillis();
            resourceUpdater.accept(resource);
            totalResourceUpdate = System.currentTimeMillis() - startResourceUpdate;
            transaction.setCommitComment(commitMessage);
            return transaction.commit();
        }
        catch (CommitException ex)
        {
            throw WrappedException.wrap(ex);
        }
        finally
        {
            transaction.close();
            long totalCommit = System.currentTimeMillis() - startCommit;
            System.out.format("checkin times: %,7dms = %,7dms + %,7dms%n%n",
                    totalCommit,
                    totalResourceUpdate,
                    totalCommit - totalResourceUpdate);
        }
    }

    public CDOCommitInfo commit(final String commitMessage, Consumer<CDOTransaction> updater) {
        long startCommit = System.currentTimeMillis();
        long totalUpdate = 0;
        CDOTransaction transaction = session.openTransaction();
        try {
            long startResourceUpdate = System.currentTimeMillis();
            updater.accept(transaction);
            totalUpdate = System.currentTimeMillis() - startResourceUpdate;
            transaction.setCommitComment(commitMessage);
            return transaction.commit();
        } catch (CommitException ex) {
            throw WrappedException.wrap(ex);
        } finally {
            transaction.close();
            long totalCommit = System.currentTimeMillis() - startCommit;
            System.out.format("checkin times: %dms = %dms + %dms\n", totalCommit, totalUpdate,
                    totalCommit - totalUpdate);
        }
    }

    public void addPackage(final EPackage ePackage) {
        if (isConnected()) {
            session.getPackageRegistry().putEPackage(ePackage);
        }
    }

    public void disconnect() {
        if (isConnected()) {
            view.close();
            session.close();
        }
    }

    private boolean isConnected() {
        return session != null && !session.isClosed();
    }
}