package org.cdlflex.cdoperformance.server;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

public class CdoServerApp implements IApplication {

    @Override
    public Object start(IApplicationContext context) throws Exception {
        MyCdoServer cdoSever = new MyCdoServer(MyCdoServer.H2_DB_CONNECTION_URL);
        cdoSever.start();
        System.out.println("Press any key to exit");
        System.in.read();
        cdoSever.stop();
        System.out.println("Done.");
        return IApplication.EXIT_OK;
    }

    @Override
    public void stop() {

    }
}
