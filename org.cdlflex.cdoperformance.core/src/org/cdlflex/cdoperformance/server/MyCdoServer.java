package org.cdlflex.cdoperformance.server;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.cdo.server.CDOServerUtil;
import org.eclipse.emf.cdo.server.IRepository;
import org.eclipse.emf.cdo.server.IStore;
import org.eclipse.emf.cdo.server.db.CDODBUtil;
import org.eclipse.emf.cdo.server.db.mapping.IMappingStrategy;
import org.eclipse.emf.cdo.server.net4j.CDONet4jServerUtil;
import org.eclipse.net4j.acceptor.IAcceptor;
import org.eclipse.net4j.db.IDBAdapter;
import org.eclipse.net4j.db.IDBConnectionProvider;
import org.eclipse.net4j.db.h2.H2Adapter;
import org.eclipse.net4j.util.container.IPluginContainer;
import org.eclipse.net4j.util.lifecycle.LifecycleUtil;
import org.h2.jdbcx.JdbcDataSource;

public class MyCdoServer {
    public final static String H2_MEM_CONNECTION_URL = "jdbc:h2:mem:%s;DB_CLOSE_DELAY=-1";
    public final static String H2_DB_CONNECTION_URL = "jdbc:h2:~/%s;DB_CLOSE_DELAY=-1";

    private final String dbConnectionUrl;
    private Map<String, RepositoryData> repositories = new HashMap<>();
    private IAcceptor acceptor;

    public MyCdoServer() {
        this(H2_MEM_CONNECTION_URL);
    }

    public MyCdoServer(String dbConnectionUrl) {
        this.dbConnectionUrl = dbConnectionUrl;
    }

    public void start() {
        CDONet4jServerUtil.prepareContainer(IPluginContainer.INSTANCE);
        IDBAdapter dbAdapter = new H2Adapter();

        Map<String, String> props = new HashMap<String, String>();
        props.put(IRepository.Props.OVERRIDE_UUID, "");
        props.put(IRepository.Props.SUPPORTING_AUDITS, "true");
        props.put(IRepository.Props.SUPPORTING_BRANCHES, "true");
        props.put(IRepository.Props.SERIALIZE_COMMITS, "true");

        addRepository("repo", props, dbAdapter);

        String description = "0.0.0.0:22233";
        acceptor = (IAcceptor)IPluginContainer.INSTANCE.getElement("org.eclipse.net4j.acceptors", "tcp", description);
    }

    private void addRepository(String repoName, Map<String, String> props, IDBAdapter dbAdapter) {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(String.format(dbConnectionUrl, repoName));
        dataSource.setUser("admin");
        dataSource.setPassword("password");

        IMappingStrategy mappingStrategy = CDODBUtil.createHorizontalMappingStrategy(true);
        IDBConnectionProvider dbConnectionProvider = dbAdapter.createConnectionProvider(dataSource);
        IStore store = CDODBUtil.createStore(mappingStrategy, dbAdapter, dbConnectionProvider);

        IRepository repository = CDOServerUtil.createRepository(repoName, store, props);
        CDOServerUtil.addRepository(IPluginContainer.INSTANCE, repository);
        repositories.put(repoName, new RepositoryData(repository, dbConnectionProvider));
    }

    public void stop() {
        LifecycleUtil.deactivate(acceptor);
        repositories.values().forEach(repo -> LifecycleUtil.deactivate(repo));
    }

    public void purge() throws SQLException {
        for (RepositoryData repositoryData: repositories.values()) {
            repositoryData.purge();
        }
    }

    static class RepositoryData {
        private IRepository repository;
        private IDBConnectionProvider dbConnectionProvider;

        RepositoryData(IRepository repository, IDBConnectionProvider dbConnectionProvider) {
            this.repository = repository;
            this.dbConnectionProvider = dbConnectionProvider;
        }

        IRepository getRepository() {
            return repository;
        }

        IDBConnectionProvider getDbConnectionProvider() {
            return dbConnectionProvider;
        }

        void purge() throws SQLException {
            dbConnectionProvider.getConnection().createStatement().execute("shutdown");
        }
    }
}
