package org.cdlflex.cdoperformance.model.CAEX.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.cdlflex.cdoperformance.model.CAEX.CAEXFile;
import org.cdlflex.cdoperformance.model.CAEX.DocumentRoot;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

public final class AmlReader {

    private AmlReader() {
        //
    }

    public static CAEXFile getAmlModel(InputStream in) throws IOException {
        XMLProcessor processor = new AmlProcessor();

        Resource resource = processor.load(in, new HashMap<>());
        return ((DocumentRoot) resource.getContents().get(0)).getCAEXFile();
    }
}
