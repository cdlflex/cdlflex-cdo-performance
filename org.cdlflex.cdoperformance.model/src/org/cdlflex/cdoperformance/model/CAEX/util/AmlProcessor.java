package org.cdlflex.cdoperformance.model.CAEX.util;

import java.util.HashMap;
import java.util.Map;

import org.cdlflex.cdoperformance.model.CAEX.CaexPackage;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLLoad;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.SAXXMLHandler;
import org.eclipse.emf.ecore.xmi.impl.XMLLoadImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * AmlProcessor provides helper methods to deserialize AML XML (CAEX) using EMF framework.
 *
 */
class AmlProcessor extends XMLProcessor {
    public AmlProcessor() {
        super();
        extendedMetaData.putPackage(null, CaexPackage.eINSTANCE);
    }

    @Override
    protected Map<String, Resource.Factory> getRegistrations() {
        if (registrations == null) {
            registrations = new HashMap<String, Resource.Factory>();
            registrations.put(STAR_EXTENSION, new AmlResourceFactory(extendedMetaData));
        }
        return registrations;
    }

    private class AmlResourceFactory extends ResourceFactoryImpl {
        private final ExtendedMetaData extendedMetaData;

        public AmlResourceFactory(ExtendedMetaData extendedMetaData) {
            this.extendedMetaData = extendedMetaData;
        }

        @Override
        public Resource createResource(URI uri) {
            XMLResource result = new XMLResourceImpl(uri) {
                @Override
                protected XMLLoad createXMLLoad() {
                    return new XMLLoadImpl(createXMLHelper()) {
                        @Override
                        protected DefaultHandler makeDefaultHandler() {
                            return new AmlDefaultHandler(resource, helper, options);
                        }
                    };
                }
            };

            result.getDefaultLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, extendedMetaData);
            result.getDefaultLoadOptions().put(XMLResource.OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);
            result.getDefaultLoadOptions().put(XMLResource.OPTION_USE_LEXICAL_HANDLER, Boolean.TRUE);

            return result;
        }
    }

    private class AmlDefaultHandler extends SAXXMLHandler {
        private static final String ADDITIONAL_INFORMATION = "AdditionalInformation";

        private int skipCount;

        public AmlDefaultHandler(XMLResource xmiResource, XMLHelper helper, Map<?, ?> options) {
            super(xmiResource, helper, options);
        }

        @Override
        @SuppressWarnings("checkstyle:parameternumber")
        public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
            if (ADDITIONAL_INFORMATION.equals(localName)) {
                skipCount++;
            }
            if (skipCount == 0) {
                super.startElement(uri, localName, qName, attributes);
            }
        }

        @Override
        public void endElement(String uri, String localName, String name) {
            if (skipCount == 0) {
                super.endElement(uri, localName, name);
            } else if (ADDITIONAL_INFORMATION.equals(localName)) {
                skipCount--;
            }
        }
    }
}
